sap.ui.define([ "sap/ui/core/UIComponent", "sap/ui/model/json/JSONModel" ],
		function(UIComponent, JSONModel) {
			"use strict";

			return UIComponent.extend(
					"com.seidorveritas.apps.vtexplain.Component", {

						metadata : {
							manifest : "json"
						},

						init : function() {

							// call the init function of the parent
							UIComponent.prototype.init.apply(this, arguments);
							// additional initialization can be done here
							var oData = {logo: jQuery.sap.getModulePath("sap.ui.core", '/') + "mimes/logo/sap_50x26.png"};
							var oModel = new JSONModel();
							oModel.setData(oData);
							this.setModel(oModel);
						}

					});
		});