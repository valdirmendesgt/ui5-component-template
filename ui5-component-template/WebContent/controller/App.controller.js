sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/json/JSONModel"],
	function (Controller, MessageToast, JSONModel) {
		"use strict";

		return Controller.extend("com.seidorveritas.apps.vtexplain.controller.App", {
			
			onInit: function() {
				var oData = {logo: jQuery.sap.getModulePath("sap.ui.core", '/') + "mimes/logo/sap_50x26.png"};
				var oModel = new JSONModel();
				oModel.setData(oData);
				this.getView().setModel(oModel);
			},
			
			onShowHello : function () {
				// read msg from i18n model

				// show message
				MessageToast.show("Teste");
			},

		});
	});